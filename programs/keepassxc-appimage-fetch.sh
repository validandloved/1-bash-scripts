#!/bin/bash
########################################################################
##
## Keepassxc AppImage fetch for Puppy
##
## Tested: Puppy Bookwormpup64
##
## Requires: bash, curl, grep, head, lynx
##
## Version: 1
##
########################################################################

## Basic deps check
APPARRAY=(curl lynx grep head)
function check_deps(){
    for APP in "${APPARRAY[@]}"
    do
        which $APP > /dev/null 2>&1
        rc=$?
        if [ $rc == 0 ]; then
            continue
        fi

		echo "deps not satisfied, please open script to verify"
        exit 1
    done
}

check_deps

## Get version number
ver=$(lynx -dump -listonly -nonumbers "https://keepassxc.org/download/#linux" | grep '.AppImage' | head -n1 | cut -d/ -f8- | cut -f1 -d"/")
verl=$(cat ./version)

## Do we need to update?
if [ "$ver" == "$verl" ]; then
   echo 'No new version detected, exiting'
   exit 0
fi

## Updating version file
echo "$ver" > ./version

## Get the latest url release
url=$(lynx -dump -listonly -nonumbers "https://keepassxc.org/download/#linux" | grep '.AppImage' | head -n1)

## Get it
wget "$url" -q --show-progress
chmod +x "./KeePassXC-$ver-x86_64.AppImage"

## Upload to Google Drive
#gupload -c "Security" "./KeePassXC-$ver-x86_64.AppImage"
